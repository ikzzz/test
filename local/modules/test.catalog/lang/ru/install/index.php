<?
$MESS["TEST_CATALOG_NAME"]           = "Тестовое задание";
$MESS["TEST_CATALOG_DESCRIPTION"]  = "Заполняет товарами торговый каталог";
$MESS["TEST_CATALOG_PARTNER_NAME"] = "Илья К.";
$MESS["TEST_CATALOG_PARTNER_URI"]  = "http://vk.ru/ikzzz";

$MESS["TEST_CATALOG_ERROR_VERSION"] = "Версия главного модуля ниже 14. Не поддерживается технология D7, необходимая модулю. Пожалуйста обновите систему.";
$MESS["TEST_CATALOG_INSTALL_TITLE"]        = "Установка модуля";
$MESS["TEST_CATALOG_UNINSTALL_TITLE"] = "Деинсталляция модуля";
$MESS["IB_TYPE_NAME"] = "инфоблок для тестового задания";
$MESS["MY_IBLOCK_PROP"] = "свойства тестового инфоблок";
$MESS["VTEST_IBLOCK_TYPE_DELETE_ERROR"] = "ошибка при удалении инфоблока, удалите вручную";

$MESS["VTEST_IBLOCK_NAME"] = "тестовый инфоблок";