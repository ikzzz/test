<?

define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS", true);
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

use Bitrix\Main\Localization\Loc;
use Bitrix\Main\HttpApplication;
use Bitrix\Main\Loader;
use Bitrix\Main\Config\Option;
Cmodule::IncludeModule('catalog');
CModule::IncludeModule('iblock');

Loc::loadMessages(__FILE__);

$request = HttpApplication::getInstance()->getContext()->getRequest();

$module_id = htmlspecialcharsbx($request["mid"] != "" ? $request["mid"] : $request["id"]);

Loader::includeModule($module_id);


$aTabs = array(
    array(
       "DIV"       => "edit",
       "TAB"       => Loc::getMessage("TEST_CATALOG_OPTIONS_TAB_NAME"),
       "TITLE"   => Loc::getMessage("TEST_CATALOG_OPTIONS_TAB_NAME"),
       "OPTIONS" => array(
            Loc::getMessage("TEST_CATALOG_OPTIONS_TAB_COMMON"),
            array(
              "key",
                Loc::getMessage("TEST_CATALOG_OPTIONS_TAB_KEY"),
                "散腿裤子",
             array("selectbox", array(
                 "散腿裤子"   => Loc::getMessage("TEST_CATALOG_OPTIONS_TAB_ONE"),
                  "散腿裤" => Loc::getMessage("TEST_CATALOG_OPTIONS_TAB_THREE"),
                    "裤子"   => Loc::getMessage("TEST_CATALOG_OPTIONS_TAB_TWO")
               ))
          )
       )
   )
);

$tabControl = new CAdminTabControl(
  "tabControl",
 $aTabs
);

$tabControl->Begin();
?>
  <form action="<? echo($APPLICATION->GetCurPage()); ?>?mid=<? echo($module_id); ?>&lang=<? echo(LANG); ?>" method="post">

  <?
   foreach($aTabs as $aTab){

       if($aTab["OPTIONS"]){

         $tabControl->BeginNextTab();

         __AdmSettingsDrawList($module_id, $aTab["OPTIONS"]);
      }
   }

   $tabControl->Buttons();
  ?>

   <input type="submit" name="apply" value="<? echo(Loc::GetMessage("TEST_CATALOG_OPTIONS_INPUT_APPLY")); ?>" class="adm-btn-save" />

   <?
   echo(bitrix_sessid_post());
 ?>

</form>
<?
$tabControl->End();

if($request->isPost() && check_bitrix_sessid()){
  $word = $_REQUEST['key'];
  $content = file_get_contents("https://cdn.asiaoptom.com/aport.php?word=".$word."&page=2");
          $res=json_decode($content, true);
          //echo "https://cdn.asiaoptom.com/aport.php?word=".$word."&page=2";
          //echo "<pre>"; var_dump($res);

            foreach ($res as $res2)
            {
              foreach ($res2 as $res3)
              {
                  foreach ($res3 as $each_item)
                  {

                      $element .= "price:".$each_item['price']."<br/>";
                      $element .= "subject:".$each_item['subject']."<br/>";
                      $element .= "img:".$each_item['img']."<br/>";
                      /*foreach ($each_item['imageList'] as $img_element)
                      {
                                    $element .= "img:".$img_element["size310x310URL"]."<br/>";
                      }*/
                      //echo $_SERVER["DOCUMENT_ROOT"]."/img.jpg";
                      $picContent = file_get_contents($each_item['img']);
                      file_put_contents("img.jpg", $picContent);
                                $el = new CIBlockElement;
                                $PROP = array();
                                $PROP['CURENT_PRICE'] = $each_item['price']; //Свойство базовой цены
                                $arLoadProductArray = array(
                                  "IBLOCK_SECTION_ID" => false,
                                  "IBLOCK_SECTION" => 0, //ID разделов
                                  "IBLOCK_ID" => 21, //ID информационного блока
                                  "PROPERTY_VALUES" => $PROP, // Передаем массив значении для свойств
                                  "NAME" => $each_item['subject'],
                                  "ACTIVE" => "Y",
                                  "PREVIEW_PICTURE" => CFile::MakeFileArray($_SERVER["DOCUMENT_ROOT"]."/bitrix/admin/img.jpg"),
                                  "DETAIL_PICTURE" => CFile::MakeFileArray($_SERVER["DOCUMENT_ROOT"]."/bitrix/admin/img.jpg"), 
                                 );
                              if($PRODUCT_ID = $el->Add($arLoadProductArray)) {
                          echo 'New ID: '.$PRODUCT_ID.'<br>';
                          }
                           else {
                            echo 'Error: '.$el->LAST_ERROR;//Результат в конце отработки
                          }
                                //ниже  заполнение цен
                                $arFields = Array(
                                    "PRODUCT_ID" => $PRODUCT_ID, //ID типа цены, которую заполняем
                                    "CATALOG_GROUP_ID" => "1",
                                    "PRICE" => $PROP['CURENT_PRICE'], //Заполнили цену свойством
                                    "CURRENCY" => "USD", //Указали валюту
                                    );
                                $res = CPrice::GetList(
                                        array(),
                                        array(
                                        "PRODUCT_ID" => $PRODUCT_ID,
                                        "CATALOG_GROUP_ID" => "1"
                                             )   
                                        );
                                if ($arr = $res->Fetch())
                                {
                                CPrice::Update($arr["ID"],$arFields); //Обновили цену
                                }
                                else
                                {
                                CPrice::Add($arFields); //Или добавили
                                }
                    //print_r($element);        
                  }
              }
            }
}
