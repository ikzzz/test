<?
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\ModuleManager;
use Bitrix\Main\Config\Option;
use Bitrix\Main\EventManager;
use Bitrix\Main\Application;
use Bitrix\Main\IO\Directory;

Loc::loadMessages(__FILE__);

class test_catalog extends CModule
{
	public function __construct(){

  		if(file_exists(__DIR__."/version.php")){

     		 $arModuleVersion = array();

     		include_once(__DIR__."/version.php");

     		  $this->MODULE_ID            = str_replace("_", ".", get_class($this));
       		$this->MODULE_VERSION       = $arModuleVersion["VERSION"];
     		  $this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
     		  $this->MODULE_NAME          = Loc::getMessage("TEST_CATALOG_NAME");
        	$this->MODULE_DESCRIPTION  = Loc::getMessage("TEST_CATALOG_DESCRIPTION");
      		$this->PARTNER_NAME     = Loc::getMessage("TEST_CATALOG_PARTNER_NAME");
        	$this->PARTNER_URI      = Loc::getMessage("TEST_CATALOG_PARTNER_URI");
 		}

   return false;
}
public function DoInstall(){

       global $APPLICATION;
        $this->AddIblockType();
        $IBLOCK_ID = $this->AddIblock();
      

        if(CheckVersion(ModuleManager::getVersion("main"), "14.00.00")){

            $this->InstallFiles();
           $this->InstallDB();

          ModuleManager::registerModule($this->MODULE_ID);

         $this->InstallEvents();
      }else{

          $APPLICATION->ThrowException(
                Loc::getMessage("TEST_CATALOG_INSTALL_ERROR_VERSION")
         );
      }

       $APPLICATION->IncludeAdminFile(
          Loc::getMessage("TEST_CATALOG_INSTALL_TITLE")." \"".Loc::getMessage("TEST_CATALOG_NAME")."\"",
          __DIR__."/step.php"
       );

      return false;
}

public function InstallFiles(){

    CopyDirFiles(
       __DIR__."/assets/scripts",
        Application::getDocumentRoot()."/bitrix/js/".$this->MODULE_ID."/",
       true,
       true
    );

  CopyDirFiles(
       __DIR__."/assets/styles",
     Application::getDocumentRoot()."/bitrix/css/".$this->MODULE_ID."/",
      true,
       true
    );

  return false;
}

public function InstallDB(){

    return false;
}

public function InstallEvents(){

    EventManager::getInstance()->registerEventHandler(
       "main",
       "OnBeforeEndBufferContent",
        $this->MODULE_ID,
        "Test\Catalog\Main",
        "appendScriptsToPage"
 );

  return false;
}

public function AddIblockType(){
 
    global $DB;
    CModule::IncludeModule("iblock");
 
    // код для типа инфоблоков
    $iblockTypeCode = 'test_module_iblock_type';
 
    // проверяем на уникальность
    $db_iblock_type = CIBlockType::GetList(
                        Array("SORT" => "ASC"),
                        Array("ID" => $iblockTypeCode)
                    );
    // если его нет - создаём
    if (!$ar_iblock_type = $db_iblock_type->Fetch()){
        $obBlocktype = new CIBlockType;
        $DB->StartTransaction();
 
        // массив полей для нового типа инфоблоков
        $arIBType = Array(
                        'ID' => $iblockTypeCode,
                        'SECTIONS' => 'N',
                        'IN_RSS' => 'N',
                        'SORT' => 100,
                        'LANG' => Array(
                            //'en' => Array(
                            //    'NAME' => GetMessage("IB_TYPE_NAME"),
                            //),
                            'ru' => Array(
                                'NAME' => GetMessage("IB_TYPE_NAME"),
                            )
                        )
                    );
 
        // создаём новый тип для инфоблоков
        $resIBT = $obBlocktype->Add($arIBType);
        if (!$resIBT){
            $DB->Rollback();
            echo 'Error: '.$obBlocktype->LAST_ERROR;
            die();
        } else {
            $DB->Commit();
        }
    } else {
        return false;
    }
 
    return $iblockType;
}

// функция для создания инфоблока
public function AddIblock(){
    CModule::IncludeModule("iblock");
 
   $iblockCode = 'test_module_ib_code'; // символьный код для инфоблока
    $iblockType = 'test_module_iblock_type'; // код типа инфоблоков
 
    $ib = new CIBlock;

    $arFieldsForIblock = Array(
      "ACTIVE" => "Y",
      "NAME" => GetMessage("VTEST_IBLOCK_NAME"),
      "CODE" => $iblockCode,
      "IBLOCK_TYPE_ID" => $iblockType,
      "SITE_ID" => "s1",
      "GROUP_ID" => Array("2" => "R"),
      "FIELDS" => Array(
        "CODE" => Array(
          "IS_REQUIRED" => "Y",
          "DEFAULT_VALUE" => Array(
            "TRANS_CASE" => "L",
            "UNIQUE" => "Y",
            "TRANSLITERATION" => "Y",
            "TRANS_SPACE" => "-",
            "TRANS_OTHER" => "-"
          )
        )
      )
    );
    // проверка на уникальность
    $resIBE = CIBlock::GetList(
                            Array(),
                            Array(
                                'TYPE' => $iblockType,
                                "CODE" => $iblockCode
                            )
                        );

    if ($ar_resIBE = $resIBE->Fetch()){
        return false;
    } else {
        return $ib->Add($arFieldsForIblock);
    }
}


public function DoUninstall(){

  global $APPLICATION;

    $this->UnInstallFiles();
    $this->UnInstallDB();
    $this->UnInstallEvents();
    $this->DelIblocks();

    ModuleManager::unRegisterModule($this->MODULE_ID);

    $APPLICATION->IncludeAdminFile(
      Loc::getMessage("TEST_CATALOG_UNINSTALL_TITLE")." \"".Loc::getMessage("TEST_CATALOG_NAME")."\"",
        __DIR__."/unstep.php"
 );

  return false;
}

public function UnInstallFiles(){

    Directory::deleteDirectory(
     Application::getDocumentRoot()."/bitrix/js/".$this->MODULE_ID
  );

  Directory::deleteDirectory(
     Application::getDocumentRoot()."/bitrix/css/".$this->MODULE_ID
 );

  return false;
}

public function UnInstallDB(){

   Option::delete($this->MODULE_ID);

    return false;
}

public function DelIblocks(){
    global $DB;
    CModule::IncludeModule("iblock");
 
    $DB->StartTransaction();
    if (!CIBlockType::Delete($this->IBLOCK_TYPE)){
        $DB->Rollback();
         
        CAdminMessage::ShowMessage(Array(
            "TYPE" => "ERROR",
            "MESSAGE" => GetMessage("VTEST_IBLOCK_TYPE_DELETE_ERROR"),
            "DETAILS" => "",
            "HTML" => true
        ));
    }
    $DB->Commit();
}



public function UnInstallEvents(){

  EventManager::getInstance()->unRegisterEventHandler(
     "main",
       "OnBeforeEndBufferContent",
       $this->MODULE_ID,
        "Test\Catalog\Main",
        "appendScriptsToPage"
 );

  return false;
}

}
